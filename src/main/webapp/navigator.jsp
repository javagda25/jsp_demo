<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<navigator>
    <ul>
        <li>
            <a href="/student-list">Lista studentów</a>
        </li>
        <li>
            <a href="/student-add">Dodaj studenta</a>
        </li>
        <li>
            <a href="/grade-list">Lista ocen</a>
        </li>
        <li>
            <a href="/">Tabliczka mnozenia</a>
        </li>
    </ul>
</navigator>