<%@ page import="com.j25.jspdemo.model.Student" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/17/19
  Time: 7:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="/style.css">
    <title>Student Add</title>
</head>
<body>
<jsp:include page="/navigator.jsp"/>
<%--if ( requestScope.studentId == null ) --%>
<%-- WSTAW W ACTION WARTOŚĆ : /student-add --%>
<%-- else --%>
<%-- WSTAW W ACTION WARTOŚĆ : /student-edit --%>
<form action="${requestScope.studentId==null ?'/student-add' : '/student-edit'}" method="post">
    <input type="hidden" name="student_id" value="${requestScope.studentId}">
    Imie: <input type="text" name="student_name" value="${requestScope.studentName}">
    <br/>
    Nazwisko: <input type="text" name="student_lastname" value="${requestScope.studentSurname}">
    <br/>
    Wiek: <input type="number" name="student_age" value="${requestScope.studentAge}">
    <br/>
    Is Alive: <input type="checkbox" name="student_isalive" ${requestScope.studentIsAlive==true ?'checked' : ''}>
    <br/>
    <br/>
    <input type="submit"><%--Guzik 'prześlij'--%>
</form>
</body>
</html>
