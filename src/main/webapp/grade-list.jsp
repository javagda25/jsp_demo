<%@ page import="com.j25.jspdemo.model.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/17/19
  Time: 7:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="/style.css">
    <title>Student List</title>
</head>
<body>
<jsp:include page="/navigator.jsp"/>
<table>
    <tr>
        <th>Id.</th>
        <th>Value</th>
        <th>Data dodania:</th>
        <th>Przedmiot</th>
        <th></th>
    </tr>
    <c:forEach var="ocena" items="${requestScope.lista_ocen}">
        <tr>
            <td>${ocena.getId()}</td>
            <td>${ocena.getValue()}</td>
            <td>${ocena.getDateAdded().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))}</td>
            <td>${ocena.getSubject()}</td>
            <td></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
