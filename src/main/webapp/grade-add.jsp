<%@ page import="com.j25.jspdemo.model.Student" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/17/19
  Time: 7:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="/style.css">
    <title>Student Add</title>
</head>
<body>
<jsp:include page="/navigator.jsp"/>

<form action="/grade-add" method="post">
    <input type="hidden" readonly name="student_to_whom_i_should_give_grade_to" value="${requestScope.studentId}">
    Ocena : <input type="number" max="6" min="2" step="0.5" name="grade_value">
    Przedmiot: <select name="grade_subject">
        <option value="ENGLISH">English</option>
        <option value="POLISH">Polish</option>
        <option value="CHEMISTRY">Chemistry</option>
    </select>
    <br/>
    <br/>
    <input type="submit"><%--Guzik 'prześlij'--%>
</form>
</body>
</html>
