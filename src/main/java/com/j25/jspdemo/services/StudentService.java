package com.j25.jspdemo.services;

import com.j25.jspdemo.database.EntityDao;
import com.j25.jspdemo.model.Student;

import java.util.List;
import java.util.Optional;

public class StudentService {

    private EntityDao entityDao = new EntityDao();

    public StudentService() {
    }

    public List<Student> findAll() {
        return entityDao.getAll(Student.class);
    }

    public void addStudent(String name, String lastname, int age, boolean isAlive) {
        entityDao.saveOrUpdate(new Student(name, lastname, age, isAlive));
    }

    public void removeStudentById(Long studentToRemoveId) {
        entityDao.delete(Student.class, studentToRemoveId);
    }

    public Optional<Student> getStudentById(Long studentToEditId) {
        return entityDao.getById(Student.class, studentToEditId);
    }

    public void update(Student studentEdited) {
        entityDao.saveOrUpdate(studentEdited);
    }
}
