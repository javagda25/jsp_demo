package com.j25.jspdemo.controller;

import com.j25.jspdemo.model.GradeSubject;
import com.j25.jspdemo.model.Student;
import com.j25.jspdemo.services.GradeService;
import com.j25.jspdemo.services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/grade-add")
public class GradeAddServlet extends HttpServlet {
    private final GradeService gradeService = new GradeService();
    private final StudentService studentService = new StudentService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long studentId = Long.parseLong(req.getParameter("studentId")); // parametr url

        req.setAttribute("studentId", studentId);

        zaladujStroneJSP(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long studentIdentifier = Long.parseLong(req.getParameter("student_to_whom_i_should_give_grade_to"));
        Optional<Student> studentOptional = studentService.getStudentById(studentIdentifier);

        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            Double gradeValue = Double.valueOf(req.getParameter("grade_value"));
            GradeSubject gradeSubject = GradeSubject.valueOf(req.getParameter("grade_subject"));

            gradeService.addGrade(student, gradeValue, gradeSubject);

            resp.sendRedirect("/grade-list?studentId=" + studentIdentifier);
        }
    }

    private void zaladujStroneJSP(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/grade-add.jsp")
                .forward(req, resp); // przekaż atrybuty do widoku JSP
        // getRequestDispatcher - musi być jsp!!!!!!
    }
}
